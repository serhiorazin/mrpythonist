from django.shortcuts import render

# Create your views here.
from cat3.models import Category


def show_genres(request):
    return render(request, "genres.html", {'genres': Category.objects.all()})