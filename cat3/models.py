# coding=utf-8
# import mptt
# from django.db import models
#
#
# class Category(models.Model):
#     title = models.CharField(u'Название', max_length=255)
#     parent = models.ForeignKey('self', blank=True, null=True, verbose_name=u'Родитель', related_name='child')
#     content = models.TextField()
#
#     def __unicode__(self):
#         return self.title
#
#
# mptt.register(Category,)


from django.db import models
from mptt.models import MPTTModel, TreeForeignKey

class Category(MPTTModel):
    title = models.CharField(max_length=50, unique=True)
    content = models.TextField()
    parent = TreeForeignKey('self', on_delete=models.CASCADE, null=True, blank=True, related_name='children')

    class MPTTMeta:
        order_insertion_by = ['title']