from __future__ import unicode_literals

from django.apps import AppConfig


class Cat3Config(AppConfig):
    name = 'cat3'
