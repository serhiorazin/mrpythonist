from django.contrib import admin
from .models import Category
from feincms.admin import tree_editor


class CategoryAdmin(tree_editor.TreeEditor):
    pass


admin.site.register(Category, CategoryAdmin)